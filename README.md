# education-advice-app

** A demo video can be found here: https://streamable.com/gmb28 **

Git clone from Bitbucket using
git clone: https://NouryJanse@bitbucket.org/NouryJanse/education-advice-app.git

## Shell commands
`npm install` to install node_module dependencies

next, run `node_modules/.bin/webpack-dev-server` (for a live-reload dev server)

simultaneously with:

`npx webpack` 
(for scss compilation)

**Navigate to:
http://localhost:8080/app/**


Known bugs:
- Webpack dev server not working with scss (no auto-reload)