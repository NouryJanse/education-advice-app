function component() {
    var element = document.createElement('div');
    element.id = "app";

    return element;
}
document.body.appendChild(component());

import Vue from 'vue';
import App from './App.vue';
const vm = new Vue({
    el: '#app',
    render: h => h(App)
});